<?php get_header(); ?>

<?php if ( get_option('chameleon_featured') == 'on' ) get_template_part('includes/featured'); ?>


	<div id="category-name">
		<div id="category-inner">
		<h3><span class="red" style="text-transform:uppercase">Shri Krsna Urja Group</span> is a manufacturing house established in <span class="red">Jaipur</span> since 1929
</h3>
        </div>
	</div> <!-- end .category-name -->


<div id="content-area" class="wrapper">


<div id="content" class="clearfix">
<!--<h2 class="border">Featured Products and Services</h2>-->
<div class="col-3">
<div class="box">
<a href="http://shrikrsna.com/product-categories/transformers/"><img src="http://shrikrsna.com/wp-content/uploads/2020/02/Transformer-Main-Page-photo-below-banner-e1582108370403.jpg" title="Transformer" alt="Transformer" /></a>
<h3 class="border">Transformer</h3>
<!-- <p>We are manufacturers of power & distribution transformers. Single Phase Transformers – upto 40KVA 33KV. Three Phase Transformers – upto 5000KVA 33KV. Special Duty Customized Transformers
</p>-->
</div>
</div>
<div class="col-3">
<div class="box">
<a href="http://shrikrsna.com/product-categories/galvanized-steel/"><img src="http://shrikrsna.com/wp-content/uploads/2020/02/Hot-Dip-Galvanized-Steel-main-page-Photo-below-Banner-e1582108200835.jpg" title="Hot Dip Galvanized Steel" alt="Hot Dip Galvanized Steel" /></a>
<h3 class="border">Hot Dip Galvanized Steel</h3>
<!-- <p>Our company specializes in designing and manufacturing Galvanized Transmission Towers & Substation Structures. 
 - Transmission Towers - upto 400KV
 - Substation Structures - upto 765KV -->
</p>
</div>
</div>
<div class="col-3">
<div class="box">
<a href="http://shrikrsna.com/solar-structures/"><img src="http://shrikrsna.com/wp-content/uploads/2020/02/Solar-Structures-main-page-photo-below-banner-e1582108309950.jpg" alt="Solar Structures" title="Solar Structures" /></a>
<h3 class="border">Solar Structures</h3>
<!-- <p>Solar Pedestals Galvanized, Cable Trays & Gratings, LT Distribution Panels, Meter Boxes, PCC Poles, Foundation Bolts & J-Hooks, Logistics -->
</p>
</div>
</div>
<div class="col-3">
<div class="box">
<a href=" http://shrikrsna.com/instrument-transformers/">
<img src="http://shrikrsna.com/wp-content/uploads/2015/06/img_smallBanner.jpg" alt="Instrument Transformers" title="Instrument Transformers" /></a>
<h3 class="border">Instrument Transformers</h3>
<!-- <p>Solar Pedestals Galvanized, Cable Trays & Gratings, LT Distribution Panels, Meter Boxes, PCC Poles, Foundation Bolts & J-Hooks, Logistics -->
</p>
</div>
</div>
<div class="clr"></div>
<div class="col-3" style="width:100%">
<div class="box">
<img src="http://shrikrsna.com/wp-content/uploads/2020/02/Others-main-page-Photo-below-banner.jpg" title="Others" alt="others" />
<h3 class="border">Others</h3>
<!-- <p>Solar Pedestals Galvanized, Cable Trays & Gratings, LT Distribution Panels, Meter Boxes, PCC Poles, Foundation Bolts & J-Hooks, Logistics -->
</p>
</div>
</div>

</div></div>
</div>
<div class="blackbox">
<div class="news-wrapper" >
<h2 class="border">Recent News & Announcements</h2>

<div class="news">
	<a href="http://shrikrsna.com/seminar-atc-losses-transformer-failure/">
		<img src="http://shrikrsna.com/wp-content/uploads/2017/02/IMG-20170201-WA0020-300x199.jpg" width="300" title="Seminar on AT&C losses & Transformer Failure" alt="Seminar on AT&C losses & Transformer Failure"/>
		<div class="newsimg">Seminar on AT&C losses & Transformer Failure
	</div>
	</a>
</div>

<div class="news"><a href="http://shrikrsna.com/ieema-workshop-transformer-quality-control/"><img src="http://shrikrsna.com/wp-content/uploads/2016/06/imagehomepageblock-e1465454066525.jpg" title="Awareness workshop on transformers electrical quality control order by IEEMA" alt="Awareness workshop on transformers electrical quality control order by IEEMA" />
<div class="newsimg">Awareness workshop on transformers electrical quality control order by IEEMA</div>
</a>
</div>
<!-- <div class="img_news" >
	<a href="http://shrikrsna.com/elecrama-feb-13-17th-2016-bangalore/">
		<img src="<?php echo get_template_directory_uri(); ?>/images/news/elecrama.jpg" alt="visit us at Our stall HL11 in Hall 1B at Elecrama Bangalore 2016" title="visit us at Our stall HL11 in Hall 1B at Elecrama Bangalore 2016"/>
		<div class="newsimg">
			Visit us at Our stall HL11 in Hall 1B at Elecrama Bangalore 2016
		</div>
	</a>
</div> -->
<div class="img_news" >
	<a href="http://shrikrsna.com/blood-donation-camp-organized-at-sksupl-by-cii-jaipur/">
		<img src="http://shrikrsna.com/wp-content/uploads/2018/06/IMG_20180615_150015-e1529485905409.jpg" alt="Blood Donation camp organized at SKSUPL by CII jaipur"/>
		<div class="newsimg">
			Blood Donation camp organized at SKSUPL by CII jaipur
		</div>
	</a>
</div> 

<div class="news">
	<img src="<?php echo get_template_directory_uri(); ?>/images/news/mp.jpg"/ alt="BIS certified under license no. CM/L-8400029213" title="BIS certified under license no. CM/L-8400029213">
	<div class="newsimg">BIS certified under license no. CM/L-8400029213 for distribution transformers upto 200KVA 11KV class as per IS1180:2014
	</div>
</div>
<div class="news">
	<a href="http://shrikrsna.com/presentation-on-best-earthing-practices-workshop-in-jaipur-by-ieema/">
		<img src="http://shrikrsna.com/wp-content/uploads/2016/11/2016-11-08-PHOTO-00000005.jpg" width="300" title="Presentation on best earthing practices workshop in jaipur by IEEMA" alt="Presentation on best earthing practices workshop in jaipur by IEEMA" />
		<div class="newsimg">Presentation on best earthing practices workshop in jaipur by IEEMA
	</div>
	</a>
</div>
</div>

</div>
<div class="clearfix"></div>
<div class="clients wrapper">
<h2 class="border">Our Clients</h2>
<marquee behavior="scroll" onMouseOver="javascript:stop();" onMouseOut="javascript:start();">
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/lnt.jpg" title="Shrikrsna Client-Larsen & Toubro" alt="Shrikrsna Client-Larsen & Toubro"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/kenya.jpg" title="Shrikrsna Client-Kenya Power" alt="Shrikrsna Client-Kenya Power"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/pgcil.jpg" title="Shrikrsna Client-Power Grid" alt="Shrikrsna Client-Power Grid"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/pspcl.jpg" title="Shrikrsna Client-Punjab State Power Corporation (PSPCL)" alt="shrikrsna Client-Punjab State Power Corporation ((PSPCL))"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/rvpn.jpg" title="Shrikrsna Client-Rajasthan Rajya Vidyut Prasaran Nigam Ltd. (RVPN)" alt="Shrikrsna Client-Rajasthan Rajya Vidyut Prasaran Nigam Ltd. (RVPN)"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/tataproduct.jpg" title="Shrikrsna Client-Tata Power" alt="Shrikrsna Client-Tata Power"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/jvvnl_header.jpg" title="Shrikrsna Client-Jaipur Vidyut Vitran Nigam Limited ,Government of Rajasthan" alt="Shrikrsna Client-Jaipur Vidyut Vitran Nigam Limited ,Government of Rajasthan"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/alstom.jpg" title="Shrikrsna Client-Alstom" alt="Shrikrsna Client-Alstom"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/wipro.jpg" title="Shrikrsna Client-Wipro" alt="Shrikrsna Client-Wipro"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/getco.jpg" title="Shrikrsna Client-Getco" alt="Shrikrsna Client-Getco"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/sterling.jpg" title="Shrikrsna Client-Sterling" alt="Shrikrsna Client-Sterling And Wilson"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/cpdcl.png" title="Shrikrsna Client-Chhattisgarh State Power Distribution " alt="Shrikrsna Client-Chhattisgarh State Power Distribution ..."/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/GAAR.png" title="Shrikrsna Client-GAAR" alt="Shrikrsna Client-GAAR"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/kec.png" title="Shrikrsna Client-KEC International Limited" alt="Shrikrsna Client-KEC International Limited"/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/UGVCL.png" title="Shrikrsna Client-UTTAR GUJARAT VIJ COMPANY LTD." alt="Shrikrsna Client-UTTAR GUJARAT VIJ COMPANY LTD."/>
<img src="<?php echo get_template_directory_uri(); ?>/images/brand/UHBVNL.png" title="Shrikrsna Client-Uttar Haryana Bijli Vitran Nigam(UHBVN)" alt="Shrikrsna Client-Uttar Haryana Bijli Vitran Nigam(UHBVN)"/>

</marquee>
</div>
 <!-- end #content-area -->

<?php get_footer(); ?>