<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69738650-1', 'auto');
  ga('send', 'pageview');

</script><div id="footer">
	<p class="center">
		<a href="http://wordpress.com/" rel="generator">Get a free blog at WordPress.com</a> <?php printf( __( 'Theme: %1$s by %2$s.', 'black-letterhead' ), 'Black-LetterHead', '<a href="http://ulyssesonline.com/" rel="designer">Ulysses Ronquillo</a>' ); ?>
	</p>
</div>
</div>

<?php wp_footer(); ?>

</body>
</html>
